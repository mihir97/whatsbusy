class Graph:
    dict = {}

    # Add edges to graph
    def addEdge(self, node_1, node_2):
        if node_1 in self.dict:
            self.dict[node_1]["out"].append(node_2)
        else:
            self.dict[node_1] = {"in": [], "out": [node_2]}

        if node_2 in self.dict:
            self.dict[node_2]["in"].append(node_1)
        else:
            self.dict[node_2] = {"in": [node_1], "out": []}

    # return all graph in for of a dictionary
    def giveEdges(self):
        return self.dict


# Function to identify router that is likely to cause the network failure
def identify_router(graph):
    # Get all edges
    g = graph.giveEdges()

    # Initialize max_connection to find node with maximum connections
    max_connections = float('-inf')

    # Initialize max_node which will contain list of node/nodes with max connections
    max_node = []

    # Iterate over the graph to find node with max connections
    for node in g:
        current_connections = len(
            g[node]["in"]) + len(g[node]["out"])
        if current_connections > max_connections:
            max_connections = current_connections
            max_node = [node]
        elif current_connections == max_connections:
            max_node.append(node)

    # Return list of nodes
    return max_node


if __name__ == "__main__":
    # Create a graph object
    graph = Graph()

    # Edges as input
    edges_1 = [[1, 2], [2, 3], [3, 5], [5, 2], [2, 1]]
    edges_2 = [[1, 3], [3, 5], [5, 6], [6, 4], [4, 5], [5, 2], [2, 6]]
    edges_3 = [[2, 4], [4, 6], [6, 2], [2, 5], [5, 6]]

    # Add edges defined above in the graph
    for edge in edges_1:
        graph.addEdge(edge[0], edge[1])
    # Print the list of router or routers with maximum connections
    print(identify_router(graph))


# Time Complexity:
#       O(n); where, n = number of nodes (routers) in the network
