def compress(inputString):

    # Finding the length of the input string to iterate over it
    l = len(inputString)

    # Checking for empty string
    if l == 0:
        return ""

    # Initializing variables
    previousChr = inputString[0]
    count = 1
    outputString = ""

    # Iterating over input to compress it
    for i in range(1, l):
        if inputString[i] == previousChr:
            count += 1
        else:
            if count > 1:
                outputString += previousChr + str(count)
            else:
                outputString += previousChr
            previousChr = inputString[i]
            count = 1

    # For last part of the string
    if count > 1:
        outputString += previousChr + str(count)
    else:
        outputString += previousChr
    return outputString


# Test cases
# If all test cases are passed then print "success" else print "error"
try:
    assert(compress("bbcceeee") == "b2c2e4")
    assert(compress("aaabbbcccaaa") == "a3b3c3a3")
    assert(compress("a") == "a")
    print("success")
except:
    print("error")


# Time Complexity:
#       O(n); where n = length of input string
# The compress function iterates over input string only once
